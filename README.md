<!--INTRODUCTION-->
<h1 align="center">
  <img align="center" height="40" width="40" alt="GIF" src="https://camo.githubusercontent.com/e8e7b06ecf583bc040eb60e44eb5b8e0ecc5421320a92929ce21522dbc34c891/68747470733a2f2f6d656469612e67697068792e636f6d2f6d656469612f6876524a434c467a6361737252346961377a2f67697068792e676966" /> The Data Wizard: Richard Taracha
</h1>

<h3> Welcome to the mystical world of Richard, a captivating data scientist with a mischievous sense of humor. With my wizardry in Python, R, and SQL, I navigate the realm of data, conjuring insights and enchanting visualizations. Beneath my enigmatic persona lies a penchant for sprinkling code with clever comments and hidden jokes. Join Richard on this magical journey through my repository of data sorcery. Prepare to be enchanted by the power of data and humor. Let the wizardry begin! 🧙‍♂️✨.</h3>

---
<!--GIFS-->
<!---
<img align="right" height="250" width="400" alt="GIF" src="https://miro.medium.com/max/1360/1*IRGHmiGsa16stedQvIaZfw.gif" />
-->

<p>  

<img align="right" width="425" height="350" src="https://user-images.githubusercontent.com/67068918/213999433-1efea580-a36c-46ff-9b6e-43c3251f9a9e.gif">
  
&nbsp;

<!--TOOLS & TECHNOLOGIES-->
- <p align="left">
  - <img align="left" width="25" height="25" src="https://user-images.githubusercontent.com/67068918/214104582-097569b5-1838-41ed-ae70-8793caf0643d.svg">Proficient in utilizing a range of programming tools such as Python, SQL, Tableau, TensorFlow, PyTorch, Numpy, Matplotlib, Seaborn, Scikit-Learn and analogous software applications, acquired through substantial experience.
  
</p>

- <p align="left">
  - <img align="left" width="25" height="25" src="https://user-images.githubusercontent.com/67068918/214105208-2e32b13b-9eb5-4e74-848f-b185ca09beba.svg">Currently, I am actively pursuing opportunities to further develop my expertise in Deep Learning and Machine Learning models by engaging in relevant tasks and activities.
</p>

- <p align="left">
  - <img align="left" width="25" height="25" src="https://user-images.githubusercontent.com/67068918/214105601-d27e49e9-9582-4d52-b232-a7fe12956a5d.svg">For a comprehensive overview of my professional background, I invite you to review my <a href="https://richardtaracha.glitch.me/" target="_top">portfolio website</a>
</p>                                                                     

- <p align="left">
  - <img align="left" width="25" height="25" src="https://user-images.githubusercontent.com/67068918/214105883-50f17bbc-47cf-4c39-8470-b1b7315c9b86.svg">Please feel free to reach out to me via email at either <strong>taracharichard@gmail.com</strong> or <strong>taracharichard@yahoo.com</strong>.
</p>         

<!---
![image_processing20191015-21835-e3o9jr](https://user-images.githubusercontent.com/67068918/213999433-1efea580-a36c-46ff-9b6e-43c3251f9a9e.gif)
-->

---
<!--SOCIAL ICONS-->
<!-- LinkedIn-->
<p align="right">
  <a href="https://www.linkedin.com/in/richard-taracha-098645a2/">
    <img align="right" alt="Richard Taracha" width="30px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/linkedin.svg" />
    
  </a> 
  
  <!-- Twitter-->
  <a href="https://twitter.com/Vycellous_Drum">
    <img align="right" alt="Richard Taracha | Twitter" width="30px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/twitter.svg" />
    
  </a>
  <!-- Whatsapp-->
  <a href="https://api.whatsapp.com/send?phone=+254706461385&text=&source=&data=&app_absent=">
    <img align="right" alt="Richard Taracha" width="30px" src="https://cdn.jsdelivr.net/npm/simple-icons@3.6.1/icons/whatsapp.svg" />
    
  </a>
  <!--GitHub-->
  <a href="https://github.com/TarachaR">
    <img align="right" alt="Richard Taracha" width="30px" src="https://cdn.jsdelivr.net/npm/simple-icons@3.6.1/icons/github.svg" />
    
  </a>
  <!--GitLab-->
  <a href="https://gitlab.com/TarachaR">
    <img align="right" alt="Richard Taracha" width="30px" src="https://cdn.jsdelivr.net/npm/simple-icons@3.6.1/icons/gitlab.svg" />
    
  </a>
  <!--TableauPublic-->
  <a href="https://public.tableau.com/profile/richard.taracha#!/?newProfile=&activeTab=0">
    <img align="right" alt="Richard Taracha" width="30px" src="https://cdn.jsdelivr.net/npm/simple-icons@3.6.1/icons/tableau.svg" />
  </a>
  <!--Medium
  <a href="https://richardtaracha.glitch.me/">
    <img align="center" alt="Richard Taracha" width="30px" src="https://user-images.githubusercontent.com/67068918/214382577-ab903585-9040-4eba-b4b8-fc1d214a504a.svg" />
  </a>-->

</p>



